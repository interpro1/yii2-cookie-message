<?php

namespace interPro\CookieMessage;

use yii\web\AssetBundle;

class CookieMessageAsset extends AssetBundle
{
    public $sourcePath = '@vendor/interpro/yii2-cookie-message/src/assets';

    public $js = [
        'js/CookieMessage.js'
    ];

    public $css = [
        'css/CookieMessage.css'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}