<?php
/*
 * @var $this \yii\web\View
 * @var $moreInfoLink array|string|null the URL for the hyperlink tag
 * @var $translationCategory string to use as translations category
 * */

use yii\helpers\Html;

?>
<div class="CookieMessageBox">
    <p>
        <?php echo Yii::t($translationCategory, 'We use cookies on our websites to help us offer you the best online experience. By continuing to use our website, you are agreeing to our use of cookies. Alternatively, you can manage them in your browser settings.') ?>

        <?php if(isset($moreInfoLink)):?>
            <?php echo Html::a(Yii::t($translationCategory, 'More information'), $moreInfoLink);?>
        <?php endif; ?>

        <?php echo Html::button(Yii::t($translationCategory, 'Accept'), ['class' => 'btn btn-xs btn-success CookieMessageOk']) ?>
    </p>
</div>
